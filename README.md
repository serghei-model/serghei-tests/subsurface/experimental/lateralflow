## Subsurface Test: Lateral Flow



This is the rainfall-infiltration test reported in Beegum etal., Brandhorst etal., (https://doi.org/10.5194/hess-25-4041-2021). It models rainfall on a sloped plane. The rain water infiltrate into the subsurface domain, exiting through the lateral boundaries and leading to variations of the groundwater table. Two simulation scenarios are established: (1) uniform delta z, (2) variable delta z.

This test problem is also extended to 3D for scaling tests on HPC. To perform scaling tests, users should run `generateinput.py` to create the required input files (the dem, the initial and boundary conditions). Users could adjust the domain width `W` in `generateinput.py` to determine the actual problem size. Other input files that do not change with problem size can be found in input-3D.


